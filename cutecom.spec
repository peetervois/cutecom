#
# spec file for package cutecom
#

Buildroot:      /home/peeter/Projektid/cutecom/_CPack_Packages/Linux/RPM/cutecom-0.51.0-1.x86_64
Name:           cutecom
Version:        0.51.0
Release:        1000
Url:            https://gitlab.com/cutecom/cutecom
BuildRequires:  cmake
BuildRequires:  gcc-c++
%if %{defined suse_version}
BuildRequires:  libqt5-qtbase-devel
%else
BuildRequires:  qt5-qtbase-devel
%endif
Summary:        Serial terminal
License:        GPL-3.0+
Group:          Applications/Communications

%define _rpmdir /home/peeter/Projektid/cutecom/_CPack_Packages/Linux/RPM
%define _rpmfilename cutecom-0.51.0-1000.x86_64.rpm
%define _unpacked_files_terminate_build 0
%define _topdir /home/peeter/Projektid/cutecom/_CPack_Packages/Linux/RPM

%description
Qt5 based serial terminal

%prep
mv $RPM_BUILD_ROOT /home/peeter/Projektid/cutecom/_CPack_Packages/Linux/RPM/tmpBBroot


%install
if [ -e $RPM_BUILD_ROOT ];
then
  rm -rf $RPM_BUILD_ROOT
fi
mv  "/home/peeter/Projektid/cutecom/_CPack_Packages/Linux/RPM/tmpBBroot" $RPM_BUILD_ROOT


%clean
rm -rf "$RPM_BUILD_ROOT"

%files
%defattr(-,root,root)
/usr/share/*
/usr/bin/cutecom

%changelog
* Fri Dec 18 2015 cyc1ingsir@gmail.com
- Version 0.30 release
* Mon Nov 30 2015 cyc1ingsir@gmail.com
- Version 0.25 reimplemented using Qt5
* Tue Oct 27 2015 cyc1ingsir@gmail.com
- version 0.23 based on Qt5 and command line sessions
* Thu Feb 23 2012 opensuse@dstoecker.de
- display correct license in about dialog
* Thu Jan 12 2012 coolo@suse.com
- change license to be in spdx.org format
* Tue Apr 26 2011 opensuse@dstoecker.de
- fix line breaking issue (patch copied from Debian)
* Tue Jan 27 2009 opensuse@dstoecker.de
- initial setup
